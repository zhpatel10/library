package institute;

import java.util.ArrayList;
import java.util.List;

public class Start {
    public static void main(String[] args)
    {
        Book book1 = new Book("The Story of My Experiments of Truth", "Mahatma Gandhi");
        Book book2 = new Book("The Wings of Fire", "APJ Abdul Kalam");
        List<Book> l1 = new ArrayList<Book>();
        l1.add(book1);
        l1.add(book2);

       Library lib=new Library(l1);
       List<Book> b1=lib.getTotalBooksLibrary();
       for(Book bk: b1)
       {
           System.out.println("Title: "+ bk.getTitle() + " and " + "Author: " + bk.getAuthor());
       }


    }

}
